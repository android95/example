package com.example.plusgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.plusgame.databinding.FragmentMinusGameBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MinusGameFragment : Fragment() {
    private lateinit var binding: FragmentMinusGameBinding
    private var resultText = "Please Select an Answer"
    private var scoreJa = Score()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =  DataBindingUtil.inflate(inflater, R.layout.fragment_minus_game, container, false)
        binding.resultText = resultText
        binding.score = scoreJa

        play()
        binding.apply {
            btnNext.setOnClickListener{
                resultText = "Please Select an Answer"
                play()
            }
        }
        binding.btnHomeMinus.setOnClickListener{
            it.findNavController().navigate(R.id.action_minusGameFragment_to_startFragment)
        }
        return binding.root
    }

    fun play() {
        binding.button1.isEnabled = true
        binding.button2.isEnabled = true
        binding.button3.isEnabled = true

        val random1: Int = Random.nextInt(10) + 1
        val random2: Int = Random.nextInt(10) + 1

        binding.apply {
            binding.textView1.setText(Integer.toString(random1))
            binding.textView2.setText(
                Integer.toString(random2)
            )
        }

        val sum = random1 - random2

        val position: Int = Random.nextInt(3) + 1

        binding.apply {
            if (position == 1) {
                button1.setText(Integer.toString(sum))
                button2.setText(Integer.toString(sum - 2))
                button3.setText(Integer.toString(sum + 2))
            } else if (position == 2) {
                button2.setText(Integer.toString(sum))
                button1.setText(Integer.toString(sum - 2))
                button3.setText(Integer.toString(sum + 2))
            } else {
                button3.setText(Integer.toString(sum))
                button2.setText(Integer.toString(sum - 2))
                button1.setText(Integer.toString(sum + 2))
            }
            button1.setOnClickListener {
                if (button1.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreJa.correct++
                    // amountCorrect.text = (amountCorrect.text.toString().toInt() + 1).toString()

                } else {
                    resultText = "Wrong"
                    scoreJa.wrong++
                    //amountWrong.text = (amountWrong.text.toString().toInt() + 1).toString()
                }
                it.isEnabled = false
                binding.invalidateAll()

            }
            button2.setOnClickListener {
                if (button2.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreJa.correct++
                    // amountCorrect.text = (amountCorrect.text.toString().toInt() + 1).toString()

                } else {
                    resultText = "Wrong"
                    scoreJa.wrong++
                    //amountWrong.text = (amountWrong.text.toString().toInt() + 1).toString()
                }
                it.isEnabled = false
                binding.invalidateAll()


            }
            button3.setOnClickListener {
                if (button3.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreJa.correct++


                } else {
                    resultText = "Wrong"
                    scoreJa.wrong++

                }
                it.isEnabled = false
                binding.invalidateAll()
            }
        }

    }
}