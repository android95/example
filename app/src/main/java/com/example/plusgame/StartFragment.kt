

package com.example.plusgame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import com.example.plusgame.databinding.FragmentPlusGameBinding
import com.example.plusgame.databinding.FragmentStartBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class StartFragment : Fragment(){
private lateinit var binding: FragmentStartBinding

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_start, container, false)

            binding.btnStart.setOnClickListener{
                it.findNavController().navigate(R.id.action_startFragment_to_startMenuFragment)}
            return binding.root

        }
}
