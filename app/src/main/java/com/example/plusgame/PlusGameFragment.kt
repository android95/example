package com.example.plusgame

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.example.plusgame.databinding.ActivityMainBinding
import com.example.plusgame.databinding.FragmentPlusGameBinding
import kotlinx.android.synthetic.main.fragment_plus_game.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusGameFragment : Fragment() {
    private lateinit var binding: FragmentPlusGameBinding
    private lateinit var plusGameViewModel: PlusGameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =  DataBindingUtil.inflate(inflater, R.layout.fragment_plus_game, container, false)
        binding.plusGameViewModel = plusGameViewModel

        play()
        binding.apply {
            btnNext.setOnClickListener{
//                plusGameViewModel.resultText.value = "Please Select an Answer"
                play()
            }
        }
        binding.btnHome.setOnClickListener{
            it.findNavController().navigate(R.id.action_plusGameFragment_to_startFragment)
        }
        return binding.root
    }
    fun disabledAllButton(){
        binding.apply {
            button1.isEnabled =false
            button2.isEnabled =false
            button3.isEnabled =false
        }
    }

    fun play() {
        binding.button1.isEnabled = true
        binding.button2.isEnabled = true
        binding.button3.isEnabled = true

        plusGameViewModel.createQuestion()
        button1.setOnClickListener {
            plusGameViewModel.checkAnswer1()
            disabledAllButton()
            binding.invalidateAll()

        }
            button2.setOnClickListener {
                plusGameViewModel.checkAnswer2()
                disabledAllButton()
                binding.invalidateAll()


            }
            button3.setOnClickListener {
                plusGameViewModel.checkAnswer3()
                disabledAllButton()
                binding.invalidateAll()
            }
        }


}