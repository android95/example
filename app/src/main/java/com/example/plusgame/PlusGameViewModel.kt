package com.example.plusgame

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class PlusGameViewModel: ViewModel() {
    private val _score = MutableLiveData<Score>()
    val score: LiveData<Score>
        get() = _score

    private val _resultText = MutableLiveData<String>()
    val resultText: LiveData<String>
        get() = _resultText

    private val _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private val _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private val _choice1 = MutableLiveData<Int>()
    val choice1: LiveData<Int>
        get() = _choice1


    private val _choice2 = MutableLiveData<Int>()
    val choice2: LiveData<Int>
        get() = _choice2

    private val _choice3 = MutableLiveData<Int>()
    val choice3: LiveData<Int>
        get() = _choice3

    private val _sum = MutableLiveData<Int>()
    val sum: LiveData<Int>
        get() = _sum

    private val _eventSelectChoice1 = MutableLiveData<Boolean>()
    val eventSelectChoice1 : LiveData<Boolean>
        get() = _eventSelectChoice1

    private val _eventSelectChoice2 = MutableLiveData<Boolean>()
    val eventSelectChoice2 : LiveData<Boolean>
        get() = _eventSelectChoice2


    private val _eventSelectChoice3 = MutableLiveData<Boolean>()
    val eventSelectChoice3 : LiveData<Boolean>
        get() = _eventSelectChoice3

    fun createQuestion() {
        _num1.value = Random.nextInt(10) + 1
        _num2.value = Random.nextInt(10) + 1
        val sum = _num1.value!! + _num2.value!!

        val position: Int = Random.nextInt(3) + 1

        if (position == 1) {
            _choice1.value = sum
            _choice2.value = sum - 2
            _choice3.value = sum + 2
        } else if (position == 2) {
            _choice2.value = sum
            _choice1.value = sum - 2
            _choice3.value = sum + 2
        } else {
            _choice3.value = sum
            _choice2.value = sum - 2
            _choice1.value = sum + 2
        }
    }

    fun checkAnswer1(){
        if (choice1 == sum) {
            _resultText.value = "Correct"
           _score.value?.addCorrect()



        } else {
            _resultText.value = "Wrong"
          _score.value?.addWrong()


        }
        _eventSelectChoice1.value = true
    }
    fun onSelectChoice1Finish(){
        _eventSelectChoice1.value = false
    }
    fun checkAnswer2(){
        if (choice2 == sum) {
            _resultText.value = "Correct"
            _score.value?.addCorrect()


        } else {
            _resultText.value = "Wrong"
            _score.value?.addWrong()
        }
    }
    fun checkAnswer3(){
        if (choice3 == sum) {
            _resultText.value = "Correct"
            _score.value?.addCorrect()


        } else {
            _resultText.value = "Wrong"
            _score.value?.addWrong()

        }
    }
    init{
        createQuestion()
        _score.value = Score()
        _resultText.value = "Please Select an Answer"
    }
}